
## 0.8.5 [07-31-2024]

* manual migration updates and deprecation updates

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!18

---

## 0.8.4 [03-26-2024]

* Changes made at 2024.03.26_14:12PM

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!17

---

## 0.8.3 [03-11-2024]

* Changes made at 2024.03.11_10:45AM

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!16

---

## 0.8.2 [02-26-2024]

* Changes made at 2024.02.26_13:08PM

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!15

---

## 0.8.1 [01-17-2024]

* deprecate adapter

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!14

---

## 0.8.0 [01-03-2024]

* Adapter Engine has been updated and the changes are being migrated to the adapter

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!12

---

## 0.7.2 [09-11-2023]

* Revert "More migration changes"

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!13

---

## 0.7.1 [05-18-2022]

* Fix systemName

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!10

---

## 0.7.0 [05-18-2022] & 0.6.0 [01-20-2022]

- Add some items to .gitignore (e.g. DS_Store) to keep them out of the repos.
- Changes to the README (some typo fixes - Add how to extend the adapter). Split the README into various markdown files (AUTH, BROKER, CALLS, ENHANCE, PROPERTIES, SUMMARY, SYSTEMINFO, TROUBLESHOOT)
- Fix the issues with Confluence in the markdowns (Tables, Lists, Links)
- Add scripts for easier authentication, removing hooks, etc
- Script changes (install script as well as database changes in other scripts)
- Double # of path vars on generic call
- Update versions of foundation (e.g. adapter-utils)
- Update npm publish so it supports https
- Update dependencies
- Add preinstall for minimist
- Fix new lint issues that came from eslint dependency change
- Add more thorough Unit tests for standard files (Package, Pronghorn, Properties (Schema and Sample)
- Add the adapter type in the package.json
- Add AdapterInfo.js script
- Add json-query dependency
- Add the propertiesDecorators.json for product encryption
- Change the name of internal IAP/Adapter methods to avoid collisions and make more obvious in Workflow - iapRunAdapterBasicGet, iapRunAdapterConnectivity, iapRunAdapterHealthcheck, iapTroubleshootAdapter, iapGetAdapterQueue, iapUnsuspendAdapter, iapSuspendAdapter, iapFindAdapterPath, iapUpdateAdapterConfiguration, iapGetAdapterWorkflowFunctions
- Add the adapter config in the database support - iapMoveAdapterEntitiesToDB
- Add standard broker calls - hasEntities, getDevice, getDevicesFiltered, isAlive, getConfig and iapGetDeviceCount
- Add genericAdapterRequest that does not use the base_path and version so that the path can be unique - genericAdapterRequestNoBasePath
- Add AdapterInfo.json
- Add systemName for documentation

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!8

---

## 0.5.4 [03-15-2021]

- migration to bring up to the latest foundation
  - Change to .eslintignore (adapter_modification directory)
  - Change to README.md (new properties, new scripts, new processes)
  - Changes to adapterBase.js (new methods)
  - Changes to package.json (new scripts, dependencies)
  - Changes to propertiesSchema.json (new properties and changes to existing)
  - Changes to the Unit test
  - Adding several test files, utils files and .generic entity
  - Fix order of scripts and dependencies in package.json
  - Fix order of properties in propertiesSchema.json
  - Update sampleProperties, unit and integration tests to have all new properties.
  - Add all new calls to adapter.js and pronghorn.json
  - Add suspend piece to older methods

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!7

---

## 0.5.3 [07-09-2020]

- Update to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!6

---

## 0.5.2 [04-21-2020]

- Changes to fix healthcheck

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!5

---

## 0.5.1 [01-15-2020]

- Update the adapter to the latest foundation

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!4

---

## 0.5.0 [11-08-2019]

- Update the adapter to the latest adapter foundation.
  - Updating to adapter-utils 4.24.3 (automatic)
  - Add sample token schemas (manual)
  - Adding placement property to getToken response schema (manual - before encrypt)
  - Adding sso default into action.json for getToken (manual - before response object)
  - Add new adapter properties for metrics & mock (save_metric, mongo and return_raw) (automatic - check place manual before stub)
  - Update sample properties to include new properties (manual)
  - Update integration test for raw mockdata (automatic)
  - Update test properties (manual)
  - Changes to artifactize (automatic)
  - Update type in sampleProperties so it is correct for the adapter (manual)
  - Update the readme (automatic)

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!3

---

## 0.4.0 [09-16-2019]

- Update the adapter to the latest adapter foundation

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!2

---

## 0.3.0 [07-30-2019] &  0.2.0 [07-17-2019]

- Migrate the adapter to the latest foundation, categorization and prepare for app artifact

See merge request itentialopensource/adapters/sd-wan/adapter-velocloud!1

---

## 0.1.1 [04-23-2019]

- Initial Commit

See commit 14e6986

---
