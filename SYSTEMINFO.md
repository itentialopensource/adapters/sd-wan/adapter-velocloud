# VMware VeloCloud Orchestrator

Vendor: VMware
Homepage: https://www.vmware.com/

Product: VMware VeloCloud Orchestrator
Product Page: https://sase.vmware.com/products/component-orchestrator

## Introduction
This adapter has been deprecated. It has been replaced by the [VeloCloud Orchestrator Adapter](https://gitlab.com/itentialopensource/adapters/adapter-velocloud_orchestrator)

## Why Integrate

## Additional Product Documentation
