/* @copyright Itential, LLC 2019 (pre-modifications) */

// Set globals
/* global describe it log pronghornProps */
/* eslint no-unused-vars: warn */
/* eslint no-underscore-dangle: warn  */
/* eslint import/no-dynamic-require:warn */

// include required items for testing & logging
const assert = require('assert');
const fs = require('fs');
const path = require('path');
const util = require('util');
const mocha = require('mocha');
const winston = require('winston');
const { expect } = require('chai');
const { use } = require('chai');
const td = require('testdouble');

const anything = td.matchers.anything();

// stub and attemptTimeout are used throughout the code so set them here
let logLevel = 'none';
const isRapidFail = false;
const isSaveMockData = false;

// read in the properties from the sampleProperties files
let adaptdir = __dirname;
if (adaptdir.endsWith('/test/integration')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 17);
} else if (adaptdir.endsWith('/test/unit')) {
  adaptdir = adaptdir.substring(0, adaptdir.length - 10);
}
const samProps = require(`${adaptdir}/sampleProperties.json`).properties;

// these variables can be changed to run in integrated mode so easier to set them here
// always check these in with bogus data!!!
samProps.stub = true;
samProps.host = 'replace.hostorip.here';
samProps.authentication.username = 'username';
samProps.authentication.password = 'password';
samProps.protocol = 'http';
samProps.port = 80;
samProps.ssl.enabled = false;
samProps.ssl.accept_invalid_cert = false;
if (samProps.request.attempt_timeout < 30000) {
  samProps.request.attempt_timeout = 30000;
}
const attemptTimeout = samProps.request.attempt_timeout;
const { stub } = samProps;

// these are the adapter properties. You generally should not need to alter
// any of these after they are initially set up
global.pronghornProps = {
  pathProps: {
    encrypted: false
  },
  adapterProps: {
    adapters: [{
      id: 'Test-velocloud',
      type: 'Velocloud',
      properties: samProps
    }]
  }
};

global.$HOME = `${__dirname}/../..`;

// set the log levels that Pronghorn uses, spam and trace are not defaulted in so without
// this you may error on log.trace calls.
const myCustomLevels = {
  levels: {
    spam: 6,
    trace: 5,
    debug: 4,
    info: 3,
    warn: 2,
    error: 1,
    none: 0
  }
};

// need to see if there is a log level passed in
process.argv.forEach((val) => {
  // is there a log level defined to be passed in?
  if (val.indexOf('--LOG') === 0) {
    // get the desired log level
    const inputVal = val.split('=')[1];

    // validate the log level is supported, if so set it
    if (Object.hasOwnProperty.call(myCustomLevels.levels, inputVal)) {
      logLevel = inputVal;
    }
  }
});

// need to set global logging
global.log = winston.createLogger({
  level: logLevel,
  levels: myCustomLevels.levels,
  transports: [
    new winston.transports.Console()
  ]
});

/**
 * Runs the common asserts for test
 */
function runCommonAsserts(data, error) {
  assert.equal(undefined, error);
  assert.notEqual(undefined, data);
  assert.notEqual(null, data);
  assert.notEqual(undefined, data.response);
  assert.notEqual(null, data.response);
}

/**
 * Runs the error asserts for the test
 */
function runErrorAsserts(data, error, code, origin, displayStr) {
  assert.equal(null, data);
  assert.notEqual(undefined, error);
  assert.notEqual(null, error);
  assert.notEqual(undefined, error.IAPerror);
  assert.notEqual(null, error.IAPerror);
  assert.notEqual(undefined, error.IAPerror.displayString);
  assert.notEqual(null, error.IAPerror.displayString);
  assert.equal(code, error.icode);
  assert.equal(origin, error.IAPerror.origin);
  assert.equal(displayStr, error.IAPerror.displayString);
}

/**
 * @function saveMockData
 * Attempts to take data from responses and place them in MockDataFiles to help create Mockdata.
 * Note, this was built based on entity file structure for Adapter-Engine 1.6.x
 * @param {string} entityName - Name of the entity saving mock data for
 * @param {string} actionName -  Name of the action saving mock data for
 * @param {string} descriptor -  Something to describe this test (used as a type)
 * @param {string or object} responseData - The data to put in the mock file.
 */
function saveMockData(entityName, actionName, descriptor, responseData) {
  // do not need to save mockdata if we are running in stub mode (already has mock data) or if told not to save
  if (stub || !isSaveMockData) {
    return false;
  }

  // must have a response in order to store the response
  if (responseData && responseData.response) {
    let data = responseData.response;

    // if there was a raw response that one is better as it is untranslated
    if (responseData.raw) {
      data = responseData.raw;

      try {
        const temp = JSON.parse(data);
        data = temp;
      } catch (pex) {
        // do not care if it did not parse as we will just use data
      }
    }

    try {
      const base = path.join(__dirname, `../../entities/${entityName}/`);
      const mockdatafolder = 'mockdatafiles';
      const filename = `mockdatafiles/${actionName}-${descriptor}.json`;

      if (!fs.existsSync(base + mockdatafolder)) {
        fs.mkdirSync(base + mockdatafolder);
      }

      // write the data we retrieved
      fs.writeFile(base + filename, JSON.stringify(data, null, 2), 'utf8', (errWritingMock) => {
        if (errWritingMock) throw errWritingMock;

        // update the action file to reflect the changes. Note: We're replacing the default object for now!
        fs.readFile(`${base}action.json`, (errRead, content) => {
          if (errRead) throw errRead;

          // parse the action file into JSON
          const parsedJson = JSON.parse(content);

          // The object update we'll write in.
          const responseObj = {
            type: descriptor,
            key: '',
            mockFile: filename
          };

          // get the object for method we're trying to change.
          const currentMethodAction = parsedJson.actions.find((obj) => obj.name === actionName);

          // if the method was not found - should never happen but...
          if (!currentMethodAction) {
            throw Error('Can\'t find an action for this method in the provided entity.');
          }

          // if there is a response object, we want to replace the Response object. Otherwise we'll create one.
          const actionResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === descriptor);

          // Add the action responseObj back into the array of response objects.
          if (!actionResponseObj) {
            // if there is a default response object, we want to get the key.
            const defaultResponseObj = currentMethodAction.responseObjects.find((obj) => obj.type === 'default');

            // save the default key into the new response object
            if (defaultResponseObj) {
              responseObj.key = defaultResponseObj.key;
            }

            // save the new response object
            currentMethodAction.responseObjects = [responseObj];
          } else {
            // update the location of the mock data file
            actionResponseObj.mockFile = responseObj.mockFile;
          }

          // Save results
          fs.writeFile(`${base}action.json`, JSON.stringify(parsedJson, null, 2), (err) => {
            if (err) throw err;
          });
        });
      });
    } catch (e) {
      log.debug(`Failed to save mock data for ${actionName}. ${e.message}`);
      return false;
    }
  }

  // no response to save
  log.debug(`No data passed to save into mockdata for ${actionName}`);
  return false;
}

// require the adapter that we are going to be using
const Velocloud = require('../../adapter');

// begin the testing - these should be pretty well defined between the describe and the it!
describe('[integration] Velocloud Adapter Test', () => {
  describe('Velocloud Class Tests', () => {
    const a = new Velocloud(
      pronghornProps.adapterProps.adapters[0].id,
      pronghornProps.adapterProps.adapters[0].properties
    );

    if (isRapidFail) {
      const state = {};
      state.passed = true;

      mocha.afterEach(function x() {
        state.passed = state.passed
        && (this.currentTest.state === 'passed');
      });
      mocha.beforeEach(function x() {
        if (!state.passed) {
          return this.currentTest.skip();
        }
        return true;
      });
    }

    describe('#class instance created', () => {
      it('should be a class with properties', (done) => {
        try {
          assert.notEqual(null, a);
          assert.notEqual(undefined, a);
          const checkId = global.pronghornProps.adapterProps.adapters[0].id;
          assert.equal(checkId, a.id);
          assert.notEqual(null, a.allProps);
          const check = global.pronghornProps.adapterProps.adapters[0].properties.healthcheck.type;
          assert.equal(check, a.healthcheckType);
          done();
        } catch (error) {
          log.error(`Test Failure: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#connect', () => {
      it('should get connected - no healthcheck', (done) => {
        try {
          a.healthcheckType = 'none';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
      it('should get connected - startup healthcheck', (done) => {
        try {
          a.healthcheckType = 'startup';
          a.connect();

          try {
            assert.equal(true, a.alive);
            done();
          } catch (error) {
            log.error(`Test Failure: ${error}`);
            done(error);
          }
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      });
    });

    describe('#healthCheck', () => {
      it('should be healthy', (done) => {
        try {
          a.healthCheck(null, (data) => {
            try {
              assert.equal(true, a.healthy);
              saveMockData('system', 'healthcheck', 'default', data);
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // broker tests
    describe('#getDevicesFiltered - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.getDevicesFiltered(opts, (data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.total);
                  assert.equal(0, data.list.length);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-velocloud-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapGetDeviceCount - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          const opts = {
            filter: {
              name: 'deviceName'
            }
          };
          a.iapGetDeviceCount((data, error) => {
            try {
              if (stub) {
                if (samProps.devicebroker.getDevicesFiltered[0].handleFailure === 'ignore') {
                  assert.equal(null, error);
                  assert.notEqual(undefined, data);
                  assert.notEqual(null, data);
                  assert.equal(0, data.count);
                } else {
                  const displayE = 'Error 400 received on request';
                  runErrorAsserts(data, error, 'AD.500', 'Test-velocloud-connectorRest-handleEndResponse', displayE);
                }
              } else {
                runCommonAsserts(data, error);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    // exposed cache tests
    describe('#iapPopulateEntityCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapPopulateEntityCache('Device', (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                done();
              } else {
                assert.equal(undefined, error);
                assert.equal('success', data[0]);
                done();
              }
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#iapRetrieveEntitiesCache - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.iapRetrieveEntitiesCache('Device', {}, (data, error) => {
            try {
              if (stub) {
                assert.equal(null, data);
                assert.notEqual(null, error);
                assert.notEqual(undefined, error);
              } else {
                assert.equal(undefined, error);
                assert.notEqual(null, data);
                assert.notEqual(undefined, data);
              }
              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
    /*
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    *** All code above this comment will be replaced during a migration ***
    ******************* DO NOT REMOVE THIS COMMENT BLOCK ******************
    -----------------------------------------------------------------------
    -----------------------------------------------------------------------
    */
    describe('#postLoginoperatorLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLoginoperatorLogin('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLoginenterpriseLogin - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLoginenterpriseLogin('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLogout - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postLogout((data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetaapiPath - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetaapiPath('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.privileges);
                assert.equal('object', typeof data.response.swagger);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfigurationcloneEnterpriseTemplate - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConfigurationcloneEnterpriseTemplate('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(7, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfigurationdeleteConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConfigurationdeleteConfiguration('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfigurationgetConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConfigurationgetConfiguration('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal(1, data.response.edgeCount);
                assert.equal('string', data.response.effective);
                assert.equal(10, data.response.id);
                assert.equal('string', data.response.modified);
                assert.equal(true, Array.isArray(data.response.modules));
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.version);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfigurationgetRoutableApplications - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConfigurationgetRoutableApplications('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.applicationClassToApplication);
                assert.equal('object', typeof data.response.applicationToApplicationClass);
                assert.equal(true, Array.isArray(data.response.applications));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDisasterRecoveryconfigureActiveForReplication - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDisasterRecoveryconfigureActiveForReplication('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDisasterRecoverydemoteActive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDisasterRecoverydemoteActive('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDisasterRecoverygetReplicationBlob - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDisasterRecoverygetReplicationBlob('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.activeAccessFromStandby);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDisasterRecoverygetReplicationStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDisasterRecoverygetReplicationStatus('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.activeAddress);
                assert.equal('string', data.response.activeReplicationAddress);
                assert.equal(true, Array.isArray(data.response.clientContact));
                assert.equal('object', typeof data.response.clientCount);
                assert.equal('COPYING_DB', data.response.drState);
                assert.equal('string', data.response.drVCOUser);
                assert.equal('string', data.response.failureDescription);
                assert.equal('string', data.response.lastDrProtectedTime);
                assert.equal('STANDALONE', data.response.role);
                assert.equal('string', data.response.roleTimestamp);
                assert.equal(true, Array.isArray(data.response.standbyList));
                assert.equal(true, Array.isArray(data.response.stateHistory));
                assert.equal('string', data.response.stateTimestamp);
                assert.equal('string', data.response.vcoIp);
                assert.equal('string', data.response.vcoReplicationIp);
                assert.equal('string', data.response.vcoUuid);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDisasterRecoveryprepareForStandby - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDisasterRecoveryprepareForStandby('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDisasterRecoverypromoteStandbyToActive - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDisasterRecoverypromoteStandbyToActive('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDisasterRecoveryremoveStandby - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDisasterRecoveryremoveStandby('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postDisasterRecoverytransitionToStandby - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postDisasterRecoverytransitionToStandby('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgedeleteEdge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgedeleteEdge('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgedeleteEdgeBgpNeighborRecords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgedeleteEdgeBgpNeighborRecords('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgesetEdgeEnterpriseConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgesetEdgeEnterpriseConfiguration('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgesetEdgeHandOffGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgesetEdgeHandOffGateways('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgesetEdgeOperatorConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgesetEdgeOperatorConfiguration('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisedeleteEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisedeleteEnterprise('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisedeleteEnterpriseGatewayRecords - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisedeleteEnterpriseGatewayRecords('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisedeleteEnterpriseNetworkAllocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisedeleteEnterpriseNetworkAllocation('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisedeleteEnterpriseService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisedeleteEnterpriseService('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseAddresses - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseAddresses('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseAlertConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseAlertConfigurations('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseAlerts - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseAlerts('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.metaData);
                assert.equal(true, Array.isArray(data.response.data));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseAllAlertsRecipients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseAllAlertsRecipients('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.emailEnabled);
                assert.equal(true, Array.isArray(data.response.emailList));
                assert.equal(true, Array.isArray(data.response.enterpriseUsers));
                assert.equal(true, data.response.mobileEnabled);
                assert.equal(true, Array.isArray(data.response.mobileList));
                assert.equal(true, data.response.smsEnabled);
                assert.equal(true, Array.isArray(data.response.smsList));
                assert.equal(false, data.response.snmpEnabled);
                assert.equal(true, Array.isArray(data.response.snmpList));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseConfigurations('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseNetworkAllocations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseNetworkAllocations('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseRouteConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseRouteConfiguration('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(5, data.response.operatorId);
                assert.equal(9, data.response.networkId);
                assert.equal(9, data.response.enterpriseId);
                assert.equal(9, data.response.edgeId);
                assert.equal(6, data.response.gatewayId);
                assert.equal(5, data.response.parentGroupId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.object);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.logicalId);
                assert.equal(true, data.response.alertsEnabled);
                assert.equal(false, data.response.operatorAlertsEnabled);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusModified);
                assert.equal('string', data.response.previousData);
                assert.equal('string', data.response.previousCreated);
                assert.equal('string', data.response.draftData);
                assert.equal('string', data.response.draftCreated);
                assert.equal('string', data.response.draftComment);
                assert.equal('string', data.response.data);
                assert.equal('string', data.response.lastContact);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseRouteTable - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseRouteTable('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.profiles));
                assert.equal(true, Array.isArray(data.response.subnets));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseServices - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseServices('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseinsertEnterpriseService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseinsertEnterpriseService('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseinsertOrUpdateEnterpriseAlertConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseinsertOrUpdateEnterpriseAlertConfigurations('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.enterpriseAlertConfigurations));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisesetEnterpriseAllAlertsRecipients - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisesetEnterpriseAllAlertsRecipients('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.emailEnabled);
                assert.equal(true, Array.isArray(data.response.emailList));
                assert.equal(true, Array.isArray(data.response.enterpriseUsers));
                assert.equal(false, data.response.mobileEnabled);
                assert.equal(true, Array.isArray(data.response.mobileList));
                assert.equal(true, data.response.smsEnabled);
                assert.equal(true, Array.isArray(data.response.smsList));
                assert.equal(false, data.response.snmpEnabled);
                assert.equal(true, Array.isArray(data.response.snmpList));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseupdateEnterpriseRoute - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseupdateEnterpriseRoute('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.eligableExits));
                assert.equal(true, Array.isArray(data.response.preferredExits));
                assert.equal('string', data.response.subnet);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseupdateEnterpriseRouteConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseupdateEnterpriseRouteConfiguration('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseupdateEnterpriseService - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseupdateEnterpriseService('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxydeleteEnterpriseProxyUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxydeleteEnterpriseProxyUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxygetEnterpriseProxyEdgeInventory - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxygetEnterpriseProxyEdgeInventory('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxygetEnterpriseProxyEnterprises - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxygetEnterpriseProxyEnterprises('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxygetEnterpriseProxyGatewayPools - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxygetEnterpriseProxyGatewayPools('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxygetEnterpriseProxyOperatorProfiles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxygetEnterpriseProxyOperatorProfiles('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxygetEnterpriseProxyUser - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postEnterpriseProxygetEnterpriseProxyUser('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxyinsertEnterpriseProxyUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxyinsertEnterpriseProxyUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxyupdateEnterpriseProxyUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxyupdateEnterpriseProxyUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseUserdeleteEnterpriseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseUserdeleteEnterpriseUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseUsergetEnterpriseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseUsergetEnterpriseUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.userType);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.officePhone);
                assert.equal('string', data.response.mobilePhone);
                assert.equal(true, data.response.isNative);
                assert.equal(false, data.response.isActive);
                assert.equal(true, data.response.isLocked);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.modified);
                assert.equal(8, data.response.roleId);
                assert.equal('string', data.response.roleName);
                assert.equal(5, data.response.enterpriseId);
                assert.equal(3, data.response.enterpriseProxyId);
                assert.equal(5, data.response.networkId);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseUserupdateEnterpriseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseUserupdateEnterpriseUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeAppLinkMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeAppLinkMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeAppLinkSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeAppLinkSeries('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeAppMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeAppMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeAppSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeAppSeries('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeCategoryMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeCategoryMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeCategorySeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeCategorySeries('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeDestMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeDestMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeDestSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeDestSeries('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeDeviceMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeDeviceMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeDeviceSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeDeviceSeries('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeLinkMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeLinkMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeLinkSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeLinkSeries('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeOsMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeOsMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeOsSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeOsSeries('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeSegmentMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeSegmentMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMetricsgetEdgeSegmentSeries - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMetricsgetEdgeSegmentSeries('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMonitoringgetAggregateEdgeLinkMetrics - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMonitoringgetAggregateEdgeLinkMetrics('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMonitoringgetAggregateEnterpriseEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMonitoringgetAggregateEnterpriseEvents('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMonitoringgetAggregates - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMonitoringgetAggregates('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(4, data.response.edgeCount);
                assert.equal('object', typeof data.response.edges);
                assert.equal(true, Array.isArray(data.response.enterprises));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMonitoringgetEnterpriseBgpPeerStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMonitoringgetEnterpriseBgpPeerStatus('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMonitoringgetEnterpriseEdgeBgpPeerStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMonitoringgetEnterpriseEdgeBgpPeerStatus('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMonitoringgetEnterpriseEdgeLinkStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMonitoringgetEnterpriseEdgeLinkStatus('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postMonitoringgetEnterpriseEdgeNvsTunnelStatus - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postMonitoringgetEnterpriseEdgeNvsTunnelStatus('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkgetNetworkConfigurations - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkgetNetworkConfigurations('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkgetNetworkEnterprises - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkgetNetworkEnterprises('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkgetNetworkGatewayPools - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkgetNetworkGatewayPools('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkgetNetworkOperatorUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkgetNetworkOperatorUsers('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOperatorUserdeleteOperatorUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOperatorUserdeleteOperatorUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOperatorUsergetOperatorUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOperatorUsergetOperatorUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(2, data.response.operatorId);
                assert.equal('string', data.response.userType);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.officePhone);
                assert.equal('string', data.response.mobilePhone);
                assert.equal(false, data.response.isNative);
                assert.equal(true, data.response.isActive);
                assert.equal(false, data.response.isLocked);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.modified);
                assert.equal(10, data.response.roleId);
                assert.equal('string', data.response.roleName);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOperatorUserinsertOperatorUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOperatorUserinsertOperatorUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(5, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(2, data.response.operatorId);
                assert.equal('string', data.response.userType);
                assert.equal('string', data.response.username);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.password);
                assert.equal('string', data.response.firstName);
                assert.equal('string', data.response.lastName);
                assert.equal('string', data.response.officePhone);
                assert.equal('string', data.response.mobilePhone);
                assert.equal(true, data.response.isNative);
                assert.equal(true, data.response.isActive);
                assert.equal(false, data.response.isLocked);
                assert.equal('string', data.response.email);
                assert.equal('string', data.response.lastLogin);
                assert.equal('string', data.response.modified);
                assert.equal(5, data.response.roleId);
                assert.equal('string', data.response.roleName);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postOperatorUserupdateOperatorUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postOperatorUserupdateOperatorUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRolecreateRoleCustomization - errors', () => {
      it('should work if integrated but since no mockdata should error when run standalone', (done) => {
        try {
          a.postRolecreateRoleCustomization('fakedata', (data, error) => {
            try {
              if (stub) {
                assert.notEqual(undefined, error);
                assert.notEqual(null, error);
                assert.equal(null, data);
                assert.equal('AD.500', error.icode);
                assert.equal('Error 400 received on request', error.IAPerror.displayString);
                const temp = 'no mock data for';
                assert.equal(0, error.IAPerror.raw_response.message.indexOf(temp));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRoledeleteRoleCustomization - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRoledeleteRoleCustomization('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRolegetUserTypeRoles - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRolegetUserTypeRoles('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcoInventoryassociateEdge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVcoInventoryassociateEdge('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.deviceSerialNumber);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postVcoInventorygetInventoryItems - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postVcoInventorygetInventoryItems('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfigurationcloneConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConfigurationcloneConfiguration('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfigurationgetConfigurationModules - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConfigurationgetConfigurationModules('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfigurationinsertConfigurationModule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConfigurationinsertConfigurationModule('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(6, data.response.id);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postConfigurationupdateConfigurationModule - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postConfigurationupdateConfigurationModule('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgeedgeCancelReactivation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgeedgeCancelReactivation('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgeedgeProvision - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgeedgeProvision('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(2, data.response.id);
                assert.equal('string', data.response.activationKey);
                assert.equal('object', typeof data.response.generatedCertificate);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgeedgeRequestReactivation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgeedgeRequestReactivation('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.activationKey);
                assert.equal('string', data.response.activationKeyExpires);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgegetEdge - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgegetEdge('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.activationKey);
                assert.equal('string', data.response.activationKeyExpires);
                assert.equal('UNASSIGNED', data.response.activationState);
                assert.equal('string', data.response.activationTime);
                assert.equal(2, data.response.alertsEnabled);
                assert.equal('string', data.response.buildNumber);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.deviceFamily);
                assert.equal('string', data.response.deviceId);
                assert.equal('string', data.response.dnsName);
                assert.equal('string', data.response.edgeHardwareId);
                assert.equal('DEGRADED', data.response.edgeState);
                assert.equal('string', data.response.edgeStateTime);
                assert.equal('CERTIFICATE_OPTIONAL', data.response.endpointPkiMode);
                assert.equal(4, data.response.enterpriseId);
                assert.equal('string', data.response.haLastContact);
                assert.equal('PENDING_INIT', data.response.haPreviousState);
                assert.equal('string', data.response.haSerialNumber);
                assert.equal('PENDING_DISSOCIATION', data.response.haState);
                assert.equal(5, data.response.id);
                assert.equal(4, data.response.isLive);
                assert.equal('string', data.response.lastContact);
                assert.equal('string', data.response.logicalId);
                assert.equal('edge1000', data.response.modelNumber);
                assert.equal('string', data.response.modified);
                assert.equal('string', data.response.name);
                assert.equal(1, data.response.operatorAlertsEnabled);
                assert.equal('string', data.response.selfMacAddress);
                assert.equal('string', data.response.serialNumber);
                assert.equal('PENDING_SERVICE', data.response.serviceState);
                assert.equal('string', data.response.serviceUpSince);
                assert.equal(4, data.response.siteId);
                assert.equal('string', data.response.softwareUpdated);
                assert.equal('string', data.response.softwareVersion);
                assert.equal('string', data.response.systemUpSince);
                assert.equal('object', typeof data.response.configuration);
                assert.equal('object', typeof data.response.enterprise);
                assert.equal(true, Array.isArray(data.response.links));
                assert.equal(true, Array.isArray(data.response.recentLinks));
                assert.equal('object', typeof data.response.site);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgegetEdgeConfigurationStack - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgegetEdgeConfigurationStack('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgeupdateEdgeAdminPassword - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgeupdateEdgeAdminPassword('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.id);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgeupdateEdgeAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgeupdateEdgeAttributes('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEdgeupdateEdgeCredentialsByConfiguration - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEdgeupdateEdgeCredentialsByConfiguration('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.actionIds));
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterprise('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(9, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(9, data.response.networkId);
                assert.equal(7, data.response.gatewayPoolId);
                assert.equal(true, data.response.alertsEnabled);
                assert.equal(false, data.response.operatorAlertsEnabled);
                assert.equal('CERTIFICATE_REQUIRED', data.response.endpointPkiMode);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.domain);
                assert.equal('string', data.response.prefix);
                assert.equal('string', data.response.logicalId);
                assert.equal('string', data.response.accountNumber);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.contactName);
                assert.equal('string', data.response.contactPhone);
                assert.equal('string', data.response.contactMobile);
                assert.equal('string', data.response.contactEmail);
                assert.equal('string', data.response.streetAddress);
                assert.equal('string', data.response.streetAddress2);
                assert.equal('string', data.response.city);
                assert.equal('string', data.response.state);
                assert.equal('string', data.response.postalCode);
                assert.equal('string', data.response.country);
                assert.equal(4, data.response.lat);
                assert.equal(2, data.response.lon);
                assert.equal('string', data.response.timezone);
                assert.equal('string', data.response.locale);
                assert.equal('string', data.response.modified);
                assert.equal('object', typeof data.response.enterpriseProxy);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseCapabilities - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseCapabilities('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.enableBGP);
                assert.equal(false, data.response.enableCosMapping);
                assert.equal(false, data.response.enableFwLogs);
                assert.equal(false, data.response.enableOSPF);
                assert.equal(false, data.response.enablePKI);
                assert.equal(true, data.response.enablePremium);
                assert.equal(true, data.response.enableServiceRateLimiting);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseEdges - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseEdges('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseGatewayHandoff - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseGatewayHandoff('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(3, data.response.enterpriseId);
                assert.equal('object', typeof data.response.value);
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.name);
                assert.equal(true, data.response.isPassword);
                assert.equal('string', data.response.dataType);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseNetworkAllocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseNetworkAllocation('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(3, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal(9, data.response.operatorId);
                assert.equal(4, data.response.networkId);
                assert.equal(5, data.response.enterpriseId);
                assert.equal(3, data.response.edgeId);
                assert.equal(9, data.response.gatewayId);
                assert.equal(1, data.response.parentGroupId);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.object);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.type);
                assert.equal('string', data.response.logicalId);
                assert.equal(false, data.response.alertsEnabled);
                assert.equal(false, data.response.operatorAlertsEnabled);
                assert.equal('string', data.response.status);
                assert.equal('string', data.response.statusModified);
                assert.equal('string', data.response.previousData);
                assert.equal('string', data.response.previousCreated);
                assert.equal('string', data.response.draftData);
                assert.equal('string', data.response.draftCreated);
                assert.equal('string', data.response.draftComment);
                assert.equal('string', data.response.data);
                assert.equal('string', data.response.lastContact);
                assert.equal('string', data.response.version);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseProperty('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(8, data.response.id);
                assert.equal(1, data.response.enterpriseId);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.value);
                assert.equal(true, data.response.isPassword);
                assert.equal('JSON', data.response.dataType);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseinsertEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseinsertEnterprise('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseinsertEnterpriseNetworkAllocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseinsertEnterpriseNetworkAllocation('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseinsertOrUpdateEnterpriseCapability - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseinsertOrUpdateEnterpriseCapability('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseinsertOrUpdateEnterpriseGatewayHandoff - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseinsertOrUpdateEnterpriseGatewayHandoff('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseinsertOrUpdateEnterpriseProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseinsertOrUpdateEnterpriseProperty('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseupdateEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseupdateEnterprise('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseupdateEnterpriseNetworkAllocation - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseupdateEnterpriseNetworkAllocation('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseupdateEnterpriseSecurityPolicy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseupdateEnterpriseSecurityPolicy('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterprisegetEnterpriseUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterprisegetEnterpriseUsers('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseinsertEnterpriseUser - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseinsertEnterpriseUser('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxygetEnterpriseProxyUsers - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxygetEnterpriseProxyUsers('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
                assert.equal('object', typeof data.response[3]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEnterpriseProxyinsertEnterpriseProxyEnterprise - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEnterpriseProxyinsertEnterpriseProxyEnterprise('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEventgetEnterpriseEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEventgetEnterpriseEvents('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postEventgetOperatorEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postEventgetOperatorEvents('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.metaData);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postFirewallgetEnterpriseFirewallLogs - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postFirewallgetEnterpriseFirewallLogs('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.metaData);
                assert.equal(true, Array.isArray(data.response.data));
                assert.equal('object', typeof data.response.rules);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGatewaydeleteGateway - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGatewaydeleteGateway('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGatewaygatewayProvision - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGatewaygatewayProvision('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('string', data.response.activationKey);
                assert.equal(6, data.response.id);
                assert.equal('string', data.response.logicalId);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postGatewayupdateGatewayAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postGatewayupdateGatewayAttributes('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postLinkQualityEventgetLinkQualityEvents - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postLinkQualityEventgetLinkQualityEvents('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response.overallLinkQuality);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkdeleteNetworkGatewayPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkdeleteNetworkGatewayPool('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkgetNetworkGateways - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkgetNetworkGateways('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkinsertNetworkGatewayPool - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkinsertNetworkGatewayPool('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postNetworkupdateNetworkGatwayPoolAttributes - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postNetworkupdateNetworkGatwayPoolAttributes('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRolesetEnterpriseDelegatedToEnterpriseProxy - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRolesetEnterpriseDelegatedToEnterpriseProxy('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(false, data.response.isDelegated);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRolesetEnterpriseDelegatedToOperator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRolesetEnterpriseDelegatedToOperator('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(false, data.response.isDelegated);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRolesetEnterpriseProxyDelegatedToOperator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRolesetEnterpriseProxyDelegatedToOperator('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(true, data.response.isDelegated);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postRolesetEnterpriseUserManagementDelegatedToOperator - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postRolesetEnterpriseUserManagementDelegatedToOperator('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(false, data.response.isDelegated);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSystemPropertygetSystemProperties - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemPropertygetSystemProperties('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal('object', typeof data.response[0]);
                assert.equal('object', typeof data.response[1]);
                assert.equal('object', typeof data.response[2]);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSystemPropertygetSystemProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemPropertygetSystemProperty('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(8, data.response.id);
                assert.equal('string', data.response.created);
                assert.equal('string', data.response.name);
                assert.equal('string', data.response.value);
                assert.equal('string', data.response.defaultValue);
                assert.equal(false, data.response.isReadOnly);
                assert.equal(true, data.response.isPassword);
                assert.equal('STRING', data.response.dataType);
                assert.equal('string', data.response.description);
                assert.equal('string', data.response.modified);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSystemPropertyinsertOrUpdateSystemProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemPropertyinsertOrUpdateSystemProperty('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSystemPropertyinsertSystemProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemPropertyinsertSystemProperty('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(123, data.response.id);
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });

    describe('#postSystemPropertyupdateSystemProperty - errors', () => {
      it('should work if integrated or standalone with mockdata', (done) => {
        try {
          a.postSystemPropertyupdateSystemProperty('fakedata', (data, error) => {
            try {
              runCommonAsserts(data, error);

              if (stub) {
                assert.equal(1, data.response.rows);
              } else {
                runCommonAsserts(data, error);
              }

              done();
            } catch (err) {
              log.error(`Test Failure: ${err}`);
              done(err);
            }
          });
        } catch (error) {
          log.error(`Adapter Exception: ${error}`);
          done(error);
        }
      }).timeout(attemptTimeout);
    });
  });
});
